from datetime import date

def absolute_days_between_dates(date1, date2):
    # Convert the dates to date objects
    date1_obj = date(*date1)
    date2_obj = date(*date2)
    
    # Calculate the absolute difference in days between the two dates
    days_difference = abs((date2_obj - date1_obj).days)
    
    return days_difference

# Input sample dates from the user
print("First Date")
year1 = int(input("Enter the Year (YYYY): "))
month1 = int(input("Enter the Month (MM): "))
day1 = int(input("Enter the Day (DD): "))

print("Second Date")
year2 = int(input("Enter the Year (YYYY): "))
month2 = int(input("Enter the Month (MM): "))
day2 = int(input("Enter the Day (DD): "))

# Dates in Tuple
date1 = (year1, month1, day1)
date2 = (year2, month2, day2)

# Calculate the number of days between the two dates
result = absolute_days_between_dates(date1, date2)

# Print the result
print("Number of days between the two dates:", result, "days")
