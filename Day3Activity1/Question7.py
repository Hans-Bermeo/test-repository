class Pet:
    def __init__(self, dog_name, dog_breed, dog_color):
        self.dog_name = dog_name
        self.dog_breed = dog_breed
        self.dog_color = dog_color
    
    def __str__(self):
        return f"I bought a {self.dog_color} {self.dog_breed} and I named my dog {self.dog_name}"

# Create an instance of the Pet class
my_dog = Pet("Max", "labrador", "black")

# Print the sample output
print(my_dog)
