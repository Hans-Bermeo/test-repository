class Parent:
    def __init__(self, name):
        self.name = name

    def display(self):
        print("Parent's name:", self.name)


class Child(Parent):
    def __init__(self, name, age):
        super().__init__(name)
        self.age = age

    def display(self):
        super().display()  # Call the display method of the parent class
        print("Child's age:", self.age)


# Create an instance of the Child class
child = Child("Sally", 21)

# Call the display method of the Child class
child.display()
