def near_thousand(num):
    # Check if the number is within 100 of 1000 or 2000
    return abs(1000 - num) <= 100 or abs(2000 - num) <= 100

# Test data
numbers = [1000, 900, 800, 2200]

# Iterate through the Test Data and print the result for each number
for number in numbers:
    print(f"{number} = {near_thousand(number)}")
