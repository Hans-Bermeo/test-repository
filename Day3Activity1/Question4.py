def future_value(amt, int_rate, years):
    # Calculation of future value
    future_val = amt * (1 + int_rate / 100) ** years
    return future_val

# Test data
amt = 10000  # Principal amount
int_rate = 3.5  # Rate of interest
years = 7  # Number of years

# Calculate the future value
result = future_value(amt, int_rate, years)

# Print the result in 2 decimal places
print("Future value:", round(result, 2))