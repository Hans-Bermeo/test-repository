def change_occurrences(sample_string):
    # Get the first character of the string
    first_char = sample_string[0]
    
    # Replace all occurrences of the first character (except the first occurrence) with '$'
    modified_string = first_char + sample_string[1:].replace(first_char, '$')
    
    return modified_string

# Test data
sample_string = 'restart'

# Get the modified string
result = change_occurrences(sample_string)

# Print the output
print("Original string:", sample_string )
print("Modified string:", result)
