from datetime import datetime, timedelta

def check_same_type_and_operate(value1, value2):
    # Check if the values are of the same type
    if type(value1) != type(value2):
        return "Values are not of the same type"
    
    # Check if the values are strings
    if isinstance(value1, str):
        return value1 + value2
    
    # Check if the values are integers
    elif isinstance(value1, int):
        return value1 * value2
    
    # Check if the values are lists
    elif isinstance(value1, list):
        return value1 + value2
    
    # Check if the values are dictionaries
    elif isinstance(value1, dict):
        return {**value1, **value2}
    
    # Check if the values are datetime objects
    elif isinstance(value1, datetime) and isinstance(value2, datetime):
        return abs(value1 - value2)

    # for Type not the same type
    # elif isinstance(value1, datetime) and isinstance(value2, timedelta):
        #return value1 + value2

    else:
        return "Unsupported data types"

# Test cases
print(check_same_type_and_operate("Hans ", "Bermeo"))  # Concatenating strings
print(check_same_type_and_operate(3, 5))  # Multiplying integers
print(check_same_type_and_operate([1, 2, 3], [4, 5, 6]))  # Concatenating lists
print(check_same_type_and_operate({"a": 1, "b": 2}, {"c": 3}))  # Merging dictionaries
print(check_same_type_and_operate(datetime(2024, 3, 18), datetime(2024, 3, 20)))  # calculate the difference between two dates

# for Type not the same type
# print(check_same_type_and_operate(datetime(2022, 1, 1), timedelta(days=1)))  # Adding timedelta to datetime
