# Sample data
input_numbers = "3, 5, 7, 23"

# Generate a list by splitting the input numbers on commas
numbers_list = input_numbers.split(',')

# Generate a tuple from the Sample Data
numbers_tuple = tuple(input_numbers.split(','))

# Generate a tuple from the List (shorcut)
# numbers_tuple = tuple(numbers_list)

# Print the list and tuple
print("List:", numbers_list)
print("Tuple:", numbers_tuple)
