# Input the user's name
name = str(input("Enter your name: "))  

# Input the user's age as an integer
age = int(input("Enter your age: "))

# Print out a message including the user's name and age
print("Hello,", name + "! You are", age, "years old.")
