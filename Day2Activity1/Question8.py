def concatenate_list_elements(input_list):
    # Join all elements in the list into a single string
    concatenated_string = ''.join(input_list)
    return concatenated_string

# Example usage:
input_list = ["Hello ", "Hans ", "Bermeo", "!"]
result = concatenate_list_elements(input_list)
print("Concatenated string:", result)

