color_list = ["Red", "Green", "White", "Black"]

# Display the first color
print("First color:", color_list[0])

# Display the last color | -1 will always get the last item on the list
print("Last color:", color_list[-1])
