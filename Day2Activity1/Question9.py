# Accept an integer input from the user
n = int(input("Enter an integer (n): "))  

# Compute the value of n + nn + nnn 
# * operator is used  for how many times needed to repeat the value of n
result = n + int(str(n) * 2) + int(str(n) * 3)

# Print the result
print("Result:", result)
