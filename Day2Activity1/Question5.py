import datetime

x = datetime.datetime.now()

print(x.strftime("The date today is %B " "%d, " "%Y "))
print(x.strftime("The time today is %H:" "%M:" "%S " "%p"))