# Input a string from the user
string = input("Enter a string: ")  

# Calculate the length of the string using the len() function
length = len(string)  

# Print the length of the string
print("Length of the ", string, " is ", length)  